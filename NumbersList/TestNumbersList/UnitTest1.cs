﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace NumbersList
{
    [TestClass]
    public class UnitTest1
    {
        /// <summary>
        /// This test method tries to add an item in an empty list of numbers
        /// </summary>
        [TestMethod]
        public void Numbers_AddNumber_AddOneItem_Success()
        {
            //given
            Numbers testNumbersObject = new Numbers();
            int valueToAdd = 4;
            List<int> expectedValue = new List<int> { valueToAdd };
            List<int> actualValue = null;

            //then
            testNumbersObject.AddNumber(valueToAdd);

            //when
            actualValue = testNumbersObject.List;
            CollectionAssert.AreEquivalent(expectedValue, actualValue);
        }

        /// <summary>
        /// This test method tries to add a collection of new items in an empty list of numbers
        /// </summary>
        [TestMethod]
        public void Numbers_AddNumber_AddACollectionOfNumber_Success()
        {
            //given
            Numbers testNumbersObject = new Numbers();
            List<int> valuesToAdd = new List<int> { 4, 8, 12 };
            List<int> expectedValue = valuesToAdd;
            List<int> actualValue = null;

            //then
            //testNumbersObject.List(valuesToAdd);//impossible car ce n'est pas un mÃ©thode mais un accesseur
            testNumbersObject.List = valuesToAdd;

            //when
            actualValue = testNumbersObject.List;
            CollectionAssert.AreEquivalent(expectedValue, actualValue);
        }

        /// <summary>
        /// This test method tries to add a collection of new items in an existing list of numbers
        /// </summary>
        [TestMethod]
        public void Numbers_AddNumber_AddACollectionOfNumbersInExistingList_Success()
        {
            //given
            List<int> initialListOfNumbers = new List<int> { 4, 8, 12 };
            Numbers testNumbersObject = new Numbers(initialListOfNumbers);
            List<int> valuesToAdd = new List<int> { 16, 56, 58 };
            List<int> expectedValue = initialListOfNumbers;
            expectedValue.AddRange(valuesToAdd);
            List<int> actualValue = null;

            //then
            //testNumbersObject.List(valuesToAdd);//impossible car ce n'est pas un mÃ©thode mais un accesseur
            testNumbersObject.List = valuesToAdd;

            //when
            actualValue = testNumbersObject.List;
            CollectionAssert.AreEquivalent(expectedValue, actualValue);
        }


        /// <summary>
        /// This test method tries to add a collection of new items, removing duplicates values
        /// </summary>
        [TestMethod]
        public void Numbers_AddNumber_AddACollectionOfNumbersInExistingListDuplicatesValues_Success()
        {
            //given
            List<int> initialListOfNumbers = new List<int> { 4, 8, 12 };
            List<int> expectedListOfNumbers = new List<int> { 4, 8, 12, 56, 58 };
            Numbers testNumbersObject = new Numbers(initialListOfNumbers);
            List<int> valuesToAdd = new List<int> { 4, 56, 58 };
            List<int> expectedValue = expectedListOfNumbers;
            List<int> actualValue = null;

            //then
            //testNumbersObject.List(valuesToAdd);//impossible car ce n'est pas un mÃ©thode mais un accesseur
            testNumbersObject.List = valuesToAdd;

            //when
            actualValue = testNumbersObject.List;
            CollectionAssert.AreEquivalent(expectedValue, actualValue);
        }

        /// <summary>
        /// This test method tries to add a collection of new items then orders items
        /// </summary>
        [TestMethod]
        public void Numbers_AddNumber_AddACollectionOfNumbersInExistingListOrderValues_Success()
        {
            //given
            List<int> initialListOfNumbers = new List<int> { 4, 8, 12 };
            List<int> expectedListOfNumbers = new List<int> { 1, 4, 8, 12, 56, 120 };
            Numbers testNumbersObject = new Numbers(initialListOfNumbers);
            List<int> valuesToAdd = new List<int> { 120, 56, 1 };
            List<int> expectedValue = expectedListOfNumbers;
            List<int> actualValue = null;

            //then
            //testNumbersObject.List(valuesToAdd);//impossible car ce n'est pas un mÃ©thode mais un accesseur
            testNumbersObject.List = valuesToAdd;

            //when
            actualValue = testNumbersObject.List;
            CollectionAssert.AreEqual(expectedValue, actualValue);
        }
    }
}
