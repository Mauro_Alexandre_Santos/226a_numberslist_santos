﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NumbersList
{
    public class Numbers
    {
        private List<int> numbersList = null;
        /// <summary>
        /// Contructor that creates the object
        /// </summary>
        public Numbers()
        {
            numbersList = new List<int>();
        }
        public Numbers(List<int> list)
        {
            numbersList = new List<int>(list);
        }
        /// <summary>
        /// Adds a number to the number list
        /// </summary>
        /// <param name="value">(int)Number to add</param>
        public void AddNumber(int value)
        {
            numbersList.Add(value);
        }
        /// <summary>
        /// Returns the last number added to the list
        /// </summary>
        /// <returns>(int) of the last number added</returns>
        public int lastvalue()
        {
            return numbersList[numbersList.Count - 1];
        }
        /// <summary>
        /// Accessor that gets and sets(without removing the existing values of) the value of numbersList
        /// </summary>
        public List<int> List
        {
            get { return numbersList; }
            set
            {
                numbersList.AddRange(value);
                numbersList = numbersList.Distinct().ToList();
                numbersList.Sort();
            }
        }
    }
}
    